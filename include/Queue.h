
#ifndef QUEUE_H
#define QUEUE_H

#include <iostream>

#define DEFAULT_CAPACITY 10

template<class T>
class Queue {
public:
    class iterator {
    public:
        iterator(T data[], size_t capacity, size_t index);

        T& operator++();
        T& operator++(int);
        T& operator*();
        bool operator!=(const Queue<T>::iterator& iter);
    private:
        T* data;
        std::size_t capacity, index;
    };
    
    Queue(size_t n=DEFAULT_CAPACITY);
    Queue(T data[], std::size_t n=DEFAULT_CAPACITY);
    ~Queue();
    Queue(const Queue& q);
    Queue(Queue&& q);

    void push(T& object);
    T pop();

    Queue<T>::iterator begin();
    Queue<T>::iterator end();
    const Queue<T>::iterator begin() const;
    const Queue<T>::iterator end() const;

private:
    void resize(size_t new_capacity);
    
    std::size_t capacity, size, front;
    T data[];
};

#include "Queue.cpp"

#endif
