#include "Pool.h"

using namespace std;
//using namespace std::chrono_literals;

template <typename ARG_TYPE, typename RETURN_TYPE> 
Pool<ARG_TYPE, RETURN_TYPE>::Pool(int processes): processes(processes) {}

template <typename ARG_TYPE, typename RETURN_TYPE> 
vector<RETURN_TYPE>
Pool<ARG_TYPE, RETURN_TYPE>::map(F func, vector<ARG_TYPE> argv) {
    size_t taskIndex = 0, argc = argv.size();
    
    Task tasks             [argc];
    vector<RETURN_TYPE> returnValues (argc);
    
    for (size_t index=0; index<argc; ++index)
        tasks[index] = {func, argv[index], &returnValues[index]};

    auto threadLoop = [&] () {
        size_t i=0;
        while (taskIndex < argc) {
            indexMutex.lock();
            i=taskIndex++;
            indexMutex.unlock();

            if (i >= argc)
                break;
            
            Task task(tasks[i]);
            *task.returnPointer = task.func(task.arg);
        }
    };

    vector<thread> threads;
    for (int proc=0; proc<processes; ++proc) {
        threads.push_back(thread(threadLoop));
    }
    
    for (thread& t : threads)
        t.join();

    return returnValues;
}
